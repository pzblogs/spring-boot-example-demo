Spring Boot 学习案例
=========================

![Spring Boot 2.0](https://img.shields.io/badge/Spring%20Boot-2.0-brightgreen.svg)
![Mysql 5.7](https://img.shields.io/badge/Mysql-5.6-blue.svg)
![JDK 1.8](https://img.shields.io/badge/JDK-1.8-brightgreen.svg)
![Maven](https://img.shields.io/badge/Maven-3.5.0-yellowgreen.svg)
![license](https://img.shields.io/badge/license-MPL--2.0-blue.svg)

## 内容说明
本仓库用于存放 Spring Boot 相关技术的各类**精选**案例代码，每个示例都经过了测试，可以直接运行，希望帮助大家快速掌握 Spring Boot 各组件的使用，相关示例代码会不定期的进行更新。

更多技术干货，可以扫描下方二维码，关注公众号：**潘志的研发笔记**，每天定时推送优质技术文章！

![](/doc/images/qrcode.jpg)

---


## 示例代码

* [spring-boot-example-cross](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-cross)：Spring Boot 解决跨域请求示例代码
* [spring-boot-example-excel](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-excel)：Spring Boot + easyexcel 实现自由导入导出示例代码
* [spring-boot-example-menu](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-menu)：Spring Boot 用户角色权限控制系统示例代码
* [spring-boot-example-jwt](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-jwt)：Spring Boot + jwt 实现单点登陆示例代码
* [spring-boot-example-email](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-email)：Spring Boot + email 实现各类邮件自动发送示例代码
* [spring-boot-example-smail](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-smail)：Spring Boot + rabbitmq + email 利用消息队列实现异步邮件自动发送示例代码
* [spring-boot-example-sensitive](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-sensitive)：Spring Boot + jackson 轻松实现接口数据脱敏示例代码
* [spring-boot-example-shardingsphere](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-sensitive)：Spring Boot + shardingsphere 实现数据字段加解密示例代码
* [spring-boot-example-schedule](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-sensitive)：Spring Boot + Schedule 实现定时任务调度示例代码
* [spring-boot-example-valid](https://gitee.com/pzblogs/spring-boot-example-demo/tree/master/spring-boot-example-valid)：Spring Boot + validator 实现全注解式的参数校验示例代码


> 如果有技术问题，或者想了解关于 Spring Boot 的其它方面应用，可以加我微信直接交流，也可以以[issues](https://gitee.com/pzblogs/spring-boot-example-demo/issues)的形式反馈给我，后续会进行完善。


![](/doc/images/person.jpg)
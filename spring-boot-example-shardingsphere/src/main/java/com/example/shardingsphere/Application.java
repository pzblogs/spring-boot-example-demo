package com.example.shardingsphere;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * Application
 * </p>
 *
 * @author panzhi
 * @since 2024/7/15
 */
@SpringBootApplication
@MapperScan("com.example.shardingsphere.mapper")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

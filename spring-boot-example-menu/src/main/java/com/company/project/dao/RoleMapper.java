package com.company.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.company.project.entity.Role;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author pzblog
 * @since 2020-06-28
 */
public interface RoleMapper extends BaseMapper<Role> {


    List<Role> findAll();

}

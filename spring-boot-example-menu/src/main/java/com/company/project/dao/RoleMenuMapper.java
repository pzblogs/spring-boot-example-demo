package com.company.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.company.project.entity.RoleMenu;

/**
 * <p>
 * 角色菜单关系表 Mapper 接口
 * </p>
 *
 * @author pzblog
 * @since 2020-06-28
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}

package com.company.project.entity.dto;

public class RoleDTO extends BaseDTO {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

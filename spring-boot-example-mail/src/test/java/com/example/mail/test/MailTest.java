package com.example.mail.test;

import com.example.mail.config.MailPushService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailTest {

    @Autowired
    private MailPushService mailPushService;

    @Test
    public void testSendMail(){
        mailPushService.sendMail("xxxx","简单标题", "测试邮件内容");
    }

    @Test
    public void testSendMail1() throws Exception {
        String sendHtml = buildHtmlContent("张三");
        mailPushService.sendMail("xxxx","简单标题", sendHtml);
    }


    @Test
    public void doSendHtmlEmail() throws Exception {
        // 获取正文内容
        String sendHtml = buildHtmlContent("张三");

        // 获取附件
        String path = MailTest.class.getClassLoader().getResource("file").getPath();
        File file = new File(path + "/Java开发手册.pdf");
        // 发送邮件
        mailPushService.sendMail("xxxx","带附件的邮件推送", sendHtml, file);
    }

    /**
     * 封装发送页面
     * @return
     * @throws Exception
     */
    private static String buildHtmlContent(String userName) throws Exception {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_23);
        configuration.setDefaultEncoding(Charset.forName("UTF-8").name());
        configuration.setClassForTemplateLoading(MailTest.class, "/templates");
        // 获取页面模版
        Template template = configuration.getTemplate("demo.ftl");
        // 动态变量替换
        Map<String,Object> map = new HashMap<>();
        map.put("userName", userName);
        String htmlStr = FreeMarkerTemplateUtils.processTemplateIntoString(template,map);
        return htmlStr;
    }

}

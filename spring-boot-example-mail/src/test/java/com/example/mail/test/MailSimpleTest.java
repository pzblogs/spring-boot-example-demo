package com.example.mail.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <p>
 * MailSimpleTest
 * </p>
 *
 * @author panzhi
 * @since 2024/6/21
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MailSimpleTest {

    @Autowired
    private JavaMailSender mailSender;

    @Test
    public void sendSimpleMail() throws Exception {
        SimpleMailMessage message = new SimpleMailMessage();
        // 配置发送者邮箱
        message.setFrom("xxxx");
        // 配置接受者邮箱
        message.setTo("xxxx");
        // 配置邮件主题
        message.setSubject("主题：简单邮件");
        // 配置邮件内容
        message.setText("测试邮件内容");
        // 发送邮件
        mailSender.send(message);
    }
}

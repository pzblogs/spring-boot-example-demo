package com.example.smail.web;

import com.example.smail.entity.Mail;
import com.example.smail.service.ProduceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * MailController
 * </p>
 *
 * @author panzhi
 * @since 2024/7/9
 */
@RestController
public class MailController {

    @Autowired
    private ProduceService produceService;


    @PostMapping("sendMail")
    public String sendMail(Mail mail) {
        boolean result = produceService.sendByAck(mail);
        return result ? "success": "fail";
    }


    @PostMapping("sendMailToDB")
    public String sendMailToDB(Mail mail) {
        boolean result = produceService.sendByAuto(mail);
        return result ? "success": "fail";
    }
}
